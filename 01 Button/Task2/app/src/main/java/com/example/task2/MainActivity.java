package com.example.task2;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener{
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button bt1 = (Button) findViewById(R.id.button1);
        Button bt2 = (Button) findViewById(R.id.button2);
        Button bt3 = (Button) findViewById(R.id.button3);
        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
        bt3.setOnClickListener(this);
    }
    @Override
   public void onClick(View b) {
       tv = (TextView) findViewById(R.id.textView);
       switch (b.getId()){
           case R.id.button1:
               tv.setText("You pressed 1st button.");
               break;
           case R.id.button2:
               tv.setText("You pressed 2st button.");
               break;
           case R.id.button3:
                tv.setText("You pressed 3st button.");
               break;

        }
    }

}
